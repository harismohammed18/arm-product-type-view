import React from 'react'
import {ProductType} from './components/product-type'
import './App.css';

function App() {
  return (
    <div>
      <ProductType/>
    </div>
  );
}

export default App;
