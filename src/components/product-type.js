import React, { useState } from 'react'

export const ProductType = () => {
    const [firstName, setFirstName] = useState("");
    const [lastName, setlastName] = useState("");

    return (
        <div class="card">
            <div class="card-body">
                <div className="row">
                    <div className="form-group col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <label htmlFor="userInputFirstName">Name</label>
                        <input type="text" className="form-control" id="userInputFirstName" required placeholder="Enter First Name" value={firstName} onChange={(e) => setFirstName(e.target.value)} />
                    </div>
                    <div className="form-group col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <label htmlFor="userInputLastName">Last Name</label>
                        <input type="text" className="form-control" id="userInputLastName" required placeholder="Enter Last Name" value={lastName} onChange={(e) => setlastName(e.target.value)} />
                    </div>
                    <button className="btn btn-primary" id="AddButton">Save</button>
                </div>
            </div>
        </div>
    );
}